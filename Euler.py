#How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?

class Euler_19():
	def __init__(self, day, start_year, end_year):
		self.start_year = start_year
		self.end_year = end_year
		self.day = day 
		self.count = 0


	def GaussAlgorithm(self, day, month, year):
		d = day
		m = (month - 3) % 12 + 1
		if m > 10:
			Y = year - 1
		else: 
			Y = year
		c = int(Y / 100)
		y = Y - c * 100

		weeks_day = (d + int(2.6 * m - 0.2) + y + int(y / 4) + int (c / 4) - 2 * c) % 7

		return int(weeks_day)

	def main(self):
		for year in xrange(self.start_year, self.end_year + 1):
			for month in xrange(1, 13):
				if self.GaussAlgorithm(1, month, year) == self.day:
					self.count += 1
		return self.count



#Find the sum of the digits in the number 100!

class Euler_20():
	def __init__(self, number):
		self.number = number
		self.adds = 0
		self.mem = {0:1, 1:1}

	def factorial(self, number):
		if number == 1:
			return 1
		elif number == 0:
			return 1
		else:
			if number in self.mem:
				return self.mem[number]
			else:
				factorial =  number * self.factorial(number - 1)
				self.mem[number] = factorial
			return self.mem[number]

	def adds_digits(self, number):
		number_string = str(number)
		for number in number_string:
			self.adds += int(number)
		return self.adds

	def answer(self):
		factoria = self.factorial(self.number)
		return self.adds_digits(factoria)




#Evaluate the sum of all the amicable numbers under 10000.

class Euler_21():
	def __init__(self, high_range):
		self.high_range = high_range
		self.final_sum = 0
		self.cache = {}

	def list_divisors(self, val):
		divisors = []
		for number in xrange(1, (val/2) + 1):
			if val % number == 0:
				divisors.append(number)
		return divisors

	def find_sum(self, li):
		add = 0
		for divisors in li:
			add += divisors
		return add

	def func(self, num):
		if num in self.cache:
			return self.cache[num]
		summed = self.find_sum(self.list_divisors(num))
		self.cache[num] = summed
		return self.cache[num]

	def main(self):
		for num1 in (xrange(1, self.high_range + 1)):
			num2 = self.func(num1)
			if num2 != num1 and self.func(num2) == num1:
				self.final_sum += num1
		return self.final_sum




#What is the total of all the name scores in the file?
class Euler_22():
	def __init__(self):
		self.final_value = 0
		self.cache = {}

	def letter_value(self, letter):
		if letter in self.cache:
			return self.cache[letter]
		value = ord(letter) - 64
		self.cache[letter] = value
		return self.cache[letter]

	def find_value(self, val):
		adds = 0
		for letter in val:
			adds += self.letter_value(letter)
		return adds


	def answer(self):
		f = open("C:\Users\samman\Desktop\class\p022_names.txt", "r")
		a = f.read()
		#print a.strip('"')
		a = a.split(",")
		for i in xrange(len(a)):
			new_i = a[i].strip('"')
			a[i] = new_i
		a.sort()
		for position in xrange(len(a)):
			self.final_value += (position + 1) * self.find_value(a[position])
		return self.final_value


#Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
class Euler_23():
	def __init__(self):
		self.abundant_numbers =set()
		self.final_adds = 0

	def is_abundant(self, num):
		divisors = self.find_divisors(num)
		sum_of_divisors = self.add(divisors)
		if num < sum_of_divisors:
			return True
		else:
			return False

	def find_divisors(self, val):
		divisors = []
		for divs in xrange(1, int(val ** 0.5) + 1):
			if val % divs == 0:
				divisors.append(divs)
				divisors.append(val / divs)
		if int((val ** 0.5)) - (val ** 0.5) == 0.0:
			divisors.remove(int(val ** 0.5))
		divisors.remove(val)
		return divisors

	def add(self, lis):
		adds = 0
		for num in lis:
			adds += num
		return adds

	def is_sum_of_abundants(self, val):
		higher = 0
		if val < 945 and (val % 2 != 0):
			return False
		for nums in self.abundant_numbers:
			if (val - nums) in self.abundant_numbers:
				return True

	def main(self):
		for i in xrange(1, 28123):
			if self.is_abundant(i):
				self.abundant_numbers.add(i)
			if not (self.is_sum_of_abundants(i)):
				self.final_adds += i
		return self.final_adds


#What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
def Euler_24():
	count1 = 0
	count2 = 0
	count3 = 0
	count4 = 0
	c = 0
	p = [1,2,3,4,5,6,7,8,9,0]
	ways = []
	temp1 = [0,0,0,0,0,0,0,0,0,0]
	temp2 = ""

	for count1 in p:
		temp1[0] = count1
		for count2 in p:
			if count2 != count1:
				temp1[1] = count2
				for count3 in p:
					if count3 != count1 and count3 != count2:
						temp1[2] = count3
						for count4 in p:
							if count4 != count1 and count4 != count2 and count4 != count3:
								temp1[3] = count4
								for count5 in p:
									if count5 != count1 and count5 != count2 and count5 != count3 and count5 != count4:
										temp1[4] = count5
										for count6 in p:
											if count6 != count1 and count6 != count2 and count6 != count3 and count6 != count4 and count6 != count5:
												temp1[5] = count6
												for count7 in p:
													if count7 != count1 and count7 != count2 and count7 != count3 and count7 != count4 and count7 != count5 and count7 != count6:
														temp1[6] = count7
														for count8 in p:
															if count8 != count1 and count8 != count2 and count8 != count3 and count8 != count4 and count8 != count5 and count8 != count6 and count8 != count7:
																temp1[7] = count8
																for count9 in p:
																	if count9 != count1 and count9 != count2 and count9 != count3 and count9 != count4 and count9 != count5 and count9 != count6 and count9 != count7 and count9 != count8:
																		temp1[8] = count9
																		for count10 in p:
																			if count10 != count1 and count10 != count2 and count10 != count3 and count10 != count4 and count10 != count5 and count10 != count6 and count10 != count7 and count10 != count8 and count10 != count9:
																				temp1[9] = count10
																				for i in temp1:
																					temp2 += str(i)
																				ways.append(int(temp2))
																				temp2 = ""


#What is the first term in the Fibonacci sequence to contain 1000 digits?
class Euler_25():
    def __init__(self):
        self.cache = {}
        self.count = 1

    def fib(self, n):
        if n == 1:
            return 1
        elif n == 2:
            return 1
        elif n in self.cache:
            return self.cache[n]
        else:
            fibo_num = self.fib(n-1) + self.fib(n-2)
            self.cache[n] = fibo_num
            return fibo_num

    def count_digits(self, n):
        str_n = str(n)
        return len(str_n)

    def main(self):
        while 1:
            num = self.fib(self.count)
            d = self.count_digits(num)
            if d == 1000:
                break
            self.count += 1
        return self.count


"for checking if subl is my default editor in git"